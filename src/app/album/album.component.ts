import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ItunesService } from '../common/itunes.service';
import { Album } from '../common/album.model';
import { Track } from '../common/track.model';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.css']
})
export class AlbumComponent implements OnInit {

  albumId: string;
  album: Album;
  tracks: Track[];
  

  constructor(private route: ActivatedRoute,
              private itunesService: ItunesService) { }

  ngOnInit(): void {
    this.route.params.subscribe(param =>  {
      this.albumId = param.id;
      this.itunesService.getAlbum(this.albumId).subscribe(foundAlbum => {
        this.album = foundAlbum;
      });
      this.itunesService.getTrackList(this.albumId).subscribe(foundTracks => {
        this.tracks = foundTracks;
      })
    })
  }

}
