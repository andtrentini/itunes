import { Component, OnInit, Input, OnDestroy } from '@angular/core';

import { faPlay, faStop, faPause } from '@fortawesome/free-solid-svg-icons';
import { PlayerService } from 'src/app/common/player.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-track-control',
  templateUrl: './track-control.component.html',
  styleUrls: ['./track-control.component.css']
})
export class TrackControlComponent implements OnInit, OnDestroy {

  @Input() previewUrl: string;

  faPlay = faPlay;
  faPause = faPause;
  isPlaying: boolean;

  stopSub: Subscription;
  endedSub: Subscription;

  constructor(private playerService: PlayerService) {
  }
  
  ngOnInit(): void {
    this.isPlaying = false;
    this.endedSub = this.playerService.trackEndedSub.subscribe(() => {
      this.isPlaying = false;
    })
    this.stopSub = this.playerService.stopTrackSub.subscribe(() => {
      this.isPlaying = false;
    })

  }

  ngOnDestroy() {
    this.endedSub.unsubscribe();
  }

  playTrack() {
    this.playerService.stopTrack();
    this.playerService.playTrack(this.previewUrl);
    this.isPlaying = true;
  }

  stopTrack() {
    this.playerService.stopTrack();
    //this.isPlaying = false;
  }

}
