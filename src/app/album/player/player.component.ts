import { Component, OnInit, ViewChild, ElementRef, OnDestroy, AfterViewInit } from '@angular/core';
import { PlayerService } from 'src/app/common/player.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent implements AfterViewInit, OnInit, OnDestroy {

  @ViewChild('htmlPlayer', {static:false}) htmlPlayer: ElementRef;
    
  playSub: Subscription;
  stopSub: Subscription;
  player: any;

  constructor(private playerService: PlayerService) { }

  ngOnInit(): void {
    this.playSub = this.playerService.playTrackSub.subscribe(previewUrl => {
      this.playTrack(previewUrl);
    });
        
    this.stopSub = this.playerService.stopTrackSub.subscribe(() => {
      this.stopTrack();
    })
  }
  
  ngAfterViewInit() {
    this.player = this.htmlPlayer.nativeElement;
  }
  
  ngOnDestroy() {
    this.playSub.unsubscribe();
    this.stopSub.unsubscribe();
  }
  
  playTrack(previewUrl) {       
    this.player.src = previewUrl;
    this.player.play();
  }

  stopTrack() {
    this.player.pause();
  }

  trackEnded() {
    this.playerService.trackEnded();
  }


}
