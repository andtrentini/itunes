import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ItunesService } from '../common/itunes.service';
import { Artist } from '../common/artist.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  @ViewChild('searchInput', {static: false}) searchInput: ElementRef;
  artists: Artist[];

  constructor(private itunesService: ItunesService,
              private router: Router) { 
    this.artists = [];
  }

  ngOnInit(): void {
    
  }

  searchArtists() {
    let searchSgtring = this.searchInput.nativeElement.value;
    this.itunesService.getArtistsList(searchSgtring).subscribe((foundArtists) => {
      this.artists = foundArtists;
    })
  }

  showArtist(artistId: string) {
    this.router.navigate(['artist', artistId]);
  }
}
