import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Artist } from './artist.model';
import { Album } from './album.model';
import { Track } from './track.model';

@Injectable({
  providedIn: 'root'
})
export class ItunesService {

  constructor(private htttpClient: HttpClient) { }

  public getArtistsList(searchString: string): Observable<Artist[]> {
    return this.htttpClient.get<any>('https://itunes.apple.com/search?limit=200&entity=musicArtist&term=' + searchString)
      .pipe(map((data) => {
        let artists: Artist[] = [];
        data.results.forEach(artist => {
          artists.push(new Artist(artist.artistId, artist.artistName, artist.primaryGenreName))
        });
        return artists;
      }))
  }

  public getArtist(artistId: string): Observable<Artist> {
    return this.htttpClient.get<any>('https://itunes.apple.com/lookup?id='+artistId)
    .pipe(map(data => {     
      return new Artist(data.results[0].artistId, data.results[0].artistName, data.results[0].primaryGenreName);      
    }))
  }

  public getAlbumsList(artistId: string): Observable<Album[]> {
    return this.htttpClient.get<any>('https://itunes.apple.com/lookup?limit=200&entity=album&id='+artistId)
    .pipe(map(data => {     
      let albums: Album[] = [];
      for (let i = 1; i < data.results.length; i++) {
        const album = data.results[i];
        albums.push(new Album(album.collectionId, album.collectionName, album.artistName, album.artworkUrl100, +album.trackCount, +album.releaseDate.substr(0, 4), album.primaryGenreName))
      }
      return albums;      
    }))
    
  }

  public getAlbum(albumId: string): Observable<Album> {
    return this.htttpClient.get<any>('https://itunes.apple.com/lookup?id='+albumId)
    .pipe(map(data => {    
      return new Album(data.results[0].collectionId, data.results[0].collectionName, data.results[0].artistName, data.results[0].artworkUrl100, +data.results[0].trackCount, +data.results[0].releaseDate.substr(0, 4), data.results[0].primaryGenreName);      
    }))
  }

  public getTrackList(albumId: string): Observable<any> {
    return this.htttpClient.get<any>('https://itunes.apple.com/lookup?limit=200&entity=song&id='+albumId)
    .pipe(map(data => {    
      let tracks: Track[] = [];
      for (let i = 1; i < data.results.length; i++) {
        const track = data.results[i];
        tracks.push(new Track(track.trackId, track.trackName, track.previewUrl, +track.trackTimeMillis, track.primaryGenreName))
      }
      return tracks;           
    }))
  }
}
