export class Track {
    id: string;
    name: string;
    previewUrl: string;
    trackTime: string;    
    genre: string;

    constructor(id: string, name: string, previewUrl: string, trackTimemills: number, genre: string) {
        this.id = id;
        this.name = name;
        this.previewUrl = previewUrl;
        let trackTimeSeconds = (trackTimemills - trackTimemills % 1000) / 1000;
        let trackTimeMinutes = (trackTimeSeconds - trackTimeSeconds % 60) / 60;
        trackTimeSeconds = trackTimeSeconds % 60;
        this.trackTime = trackTimeMinutes + ':';
        if (trackTimeSeconds < 10) {
            this.trackTime += '0'
        }
        this.trackTime += trackTimeSeconds;       
        this.genre = genre;
    }    
}
