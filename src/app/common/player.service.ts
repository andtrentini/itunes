import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  playTrackSub = new Subject<string>();
  stopTrackSub = new Subject();
  trackEndedSub = new Subject();

  constructor() { }

  playTrack(previewUrl: string) {    
    this.playTrackSub.next(previewUrl);
  }

  stopTrack() {
    this.stopTrackSub.next();
  }

  trackEnded() {
    this.trackEndedSub.next();
  }
}
