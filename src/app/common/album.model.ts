export class Album {
    id: string;
    name: string;
    artistName: string;
    img: string;
    trackCount: number;
    releaseYear: number;
    genre: string;

    constructor(id: string, name: string, artistName: string, img: string, trackCount: number, releaseYear: number, genre: string) {
        this.id = id;
        this.name = name;
        this.artistName = artistName;
        this.img = img;
        this.trackCount = trackCount,
        this.releaseYear = releaseYear;
        this.genre = genre;
    }
}
