export class Artist {
    id: string;
    name: string;
    genre: string;

    constructor(id: string, name: string, genre: string) {
        this.id = id;
        this.name = name;
        this.genre = genre;
    }
}
