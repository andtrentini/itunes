import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ItunesService } from '../common/itunes.service';
import { Album } from '../common/album.model';
import { Artist } from '../common/artist.model';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.css']
})
export class ArtistComponent implements OnInit {

  artistId: string;
  artist: Artist;
  albums: Album[];

  constructor(private route: ActivatedRoute,
              private router: Router,
              private itunesService: ItunesService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.artistId = params.id;
      this.itunesService.getArtist(this.artistId).subscribe(foundArtist => {
        this.artist = foundArtist;
      })
      this.itunesService.getAlbumsList(this.artistId).subscribe(foundAlbums => {
        this.albums = foundAlbums;
      })
    })
  }

  showAlbum(albumId: string) {
    this.router.navigate(['album', albumId]);
  }
}
